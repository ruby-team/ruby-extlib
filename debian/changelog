ruby-extlib (0.9.16-3) UNRELEASED; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 12 Sep 2021 05:49:12 -0000

ruby-extlib (0.9.16-2) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Fix day-of-week for changelog entry 0.9.13-1.

  [ Chris Hofstaedtler ]
  * Remove unused ruby-setup build dependency
  * Update Standards-Version to 4.5.0, no changes needed

 -- Chris Hofstaedtler <zeha@debian.org>  Mon, 31 Aug 2020 13:03:08 +0000

ruby-extlib (0.9.16-1) unstable; urgency=medium

  * Team upload
  * Imported Upstream version 0.9.16
  * Update debian/watch. Thanks Bart Martens
  * Drop transitional packages (Closes: #735701)
  * Bump standards-Version to 3.9.6 (no changes needed)
  * Point Vcs-* fields to Git repos
  * Set the Ruby team as the maintainer
  * Update homepage to use GitHub
  * Set format source to 3.0 (quilt)
  * Convert copyright file to copyright-format 1.0

 -- Cédric Boutillier <boutil@debian.org>  Wed, 27 May 2015 00:41:53 +0200

ruby-extlib (0.9.15-3) unstable; urgency=high

  * Team upload.
  * Import patches 633974b2759d9b92 and 4540e7102b803624 from uptream
    to remove symbol and YAML coercion from the XML parser. [CVE-2013-0156]
    (Closes: #697895)

 -- Cédric Boutillier <boutil@debian.org>  Fri, 11 Jan 2013 18:15:39 +0100

ruby-extlib (0.9.15-2) unstable; urgency=low

  * Add full text of the Ruby licence.

 -- Tollef Fog Heen <tfheen@debian.org>  Wed, 02 May 2012 17:22:04 +0200

ruby-extlib (0.9.15-1) unstable; urgency=low

  * Update to new Ruby policy
    - rename source package.  Closes: #670834
  * Bump debhelper compat version
  * Disable rubygems.rb hacks, as they aren't needed for the versions of
    rubygems we're likely to encounter.

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 10 Apr 2012 20:49:48 +0200

libextlib-ruby (0.9.13-2) unstable; urgency=low

  * std-ver -> 3.8.4. No changes needed.
  * Switch to ruby 1.9.1. Closes: #569866.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sat, 20 Feb 2010 19:06:29 +0100

libextlib-ruby (0.9.13-1) unstable; urgency=low

  * New upstream release
    - Removes rubygems require (Closes: Bug541404)
  * Update watchfile to use githubredir
  * Minor grammatical changes to short description
  * Switch section to ruby from libs
  * Bump standards revision to 3.8.3
  * Move ruby-pkg-tools to build-depends
  * Bump debhelper version from 4 to 5
  * Move History.txt from docs to changelogs

 -- Bryan McLellan <btm@loftninjas.org>  Tue, 10 Nov 2009 12:39:09 -0700

libextlib-ruby (0.9.10-2) unstable; urgency=low

  * Update package descriptions (Closes: Bug#520211)

 -- Bryan McLellan <btm@loftninjas.org>  Wed, 18 Mar 2009 11:33:30 -0700

libextlib-ruby (0.9.10-1) unstable; urgency=low

  * Initial release (Closes: Bug#513295)

 -- Bryan McLellan <btm@loftninjas.org>  Mon, 26 Jan 2009 20:38:53 -0800
